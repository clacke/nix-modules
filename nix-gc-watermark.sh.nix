{ gawk
, bash
, coreutils
, nix
, writeScript
}:

writeScript "nix-gc-watermark.sh" ''
#! ${bash}/bin/bash

set -e
set -u

# LOW_MARK  # Only run when free space is this low (KiB)
# HIGH_MARK # Only clean up to this level of free space (KiB)

free_space=$(${coreutils}/bin/df -k /nix | tail -n 1 | ${gawk}/bin/awk '{ print $4 }')

if (( free_space > LOW_MARK )); then exit 0; fi

missing_space=$((HIGH_MARK - free_space))
exec ${nix}/bin/nix-collect-garbage --max-freed $((1024 * missing_space))
''
