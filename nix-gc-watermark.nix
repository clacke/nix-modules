{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.nix.watermarks;

  watermarkType = types.submodule { options = {
    low = mkOption {
      type = types.int;
      description = ''
        If available space is lower than this (KiB), gc will run.
      '';
    };
    high = mkOption {
      type = types.int;
      description = ''
        When gc runs, it will stop when this much space (KiB) is available.
      '';
    };
    startAt = mkOption {
      default = "03:15";
      type = types.str;
      description = ''
        Specification (in the format described by
        <citerefentry><refentrytitle>systemd.time</refentrytitle>
        <manvolnum>7</manvolnum></citerefentry>) of the time at
        which the garbage collector will run.
      '';
    };
  };};
  makeServices = watermarks: makeServiceEntries 0 watermarks {};
  makeServiceEntries = index: watermarks: done: if watermarks == [] then done else let
    current = builtins.head watermarks;
    name = "nix-gc-watermark-${toString index}";
    script = import ./nix-gc-watermark.sh.nix { inherit (pkgs) bash coreutils gawk nix writeScript; };
    value = {
      description = "Nix Garbage Collector with watermarks ${toString current.low}--${toString current.high}";
      script = "exec ${script}";
      serviceConfig = {
        Environment = "LOW_MARK=${toString current.low} HIGH_MARK=${toString current.high}";
      };
      startAt = current.startAt;
    };
  in
    makeServiceEntries (index + 1) (builtins.tail watermarks) (done // { "${name}" = value; });
in
{
  options.nix.watermarks = mkOption { type = types.listOf watermarkType; };
  config = {
    systemd.services = makeServices cfg;
  };
}
